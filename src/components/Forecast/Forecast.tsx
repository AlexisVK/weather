import ForecastDay from './ForecastDay';
import { ForecastType } from '../../types';

interface ForecastProps {
    forecast: ForecastType[];
    selected: ForecastType;
    onSelect: (day: ForecastType) => void;
}

const Forecast: React.FC<ForecastProps> = ({ forecast, selected, onSelect }) => {
    const slicedForecast = forecast.length > 7 ? forecast.slice(0, 7) : forecast;

    return (
        <div className="forecast">
            { slicedForecast.map((dayForecast) => {
                return <ForecastDay forecast={dayForecast} key={dayForecast.id} selected={dayForecast.id === selected.id} onClick={() => onSelect(dayForecast)}/>
            }) }
        </div>
    );
}

export default Forecast;
