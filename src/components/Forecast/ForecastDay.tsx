import { ForecastType } from '../../types';
import { getWeekDay } from '../../utils';
export interface ForecastDayProps {
    forecast: ForecastType;
    selected: boolean;
    onClick: (id: string) => void;
}

const ForecastDay: React.FC<ForecastDayProps> = ({ forecast, selected = false, onClick }) => {
    const { type, temperature, day, id } = forecast;
    const weekDay = getWeekDay(day);

    return (
        <div className={`day ${type} ${selected ? 'selected' : ''}`} onClick={() => onClick(id)}>
            <p>{ weekDay }</p>
            <span>{ temperature }</span>
        </div>
    )
}

export default ForecastDay;
