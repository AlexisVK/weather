import classNames from 'classnames';

interface CheckboxProp {
    onClick?: () => void;
    isChecked?: boolean;
}

const Checkbox: React.FC<CheckboxProp> = ({children, onClick, isChecked = false}) => {
    const className = classNames('checkbox', {
        'selected': isChecked
    });

    return (
        <span className={className} onClick={onClick}>{children}</span>
    )
}

export default Checkbox;
