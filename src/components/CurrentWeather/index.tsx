import { ForecastType } from '../../types'
import { getWeekDay, getDayMonth } from '../../utils';
interface CurrentWeatherProps {
    currentWeather: ForecastType;
}

const CurrentWeather: React.FC<CurrentWeatherProps> = ({ currentWeather }) => {
    const { temperature, day, rain_probability, humidity, type } = currentWeather;
    const weekDay = getWeekDay(day);
    const date = getDayMonth(day);

    return (
        <>
            <div className="head">
                <div className={`icon ${type}`}></div>
                <div className="current-date">
                    <p>{ weekDay }</p>
                    <span>{ date }</span>
                </div>
            </div>
            <div className="current-weather">
                <p className="temperature">{temperature}</p>
                <p className="meta">
                    <span className="rainy">%{rain_probability}</span>
                    <span className="humidity">%{humidity}</span>
                </p>
            </div>
        </>
    )
}

export default CurrentWeather;
