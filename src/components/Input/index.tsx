import React from 'react';

interface InputProps {
    value?: number;
    name: string;
    onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Input: React.FC<InputProps> = ({children, name, value, onChange}) => {
    return (
        <p className="custom-input">
            <label htmlFor={name}>{children}</label>
            <input id={name} type="number" value={value} onChange={onChange} />
        </p>
    )
}

export default Input;
