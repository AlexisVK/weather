const Filter: React.FC= ({children}) => {
    return (
        <div className="filter">{children}</div>
    )
}

export default Filter;
