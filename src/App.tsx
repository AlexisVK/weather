import { useState } from 'react';
import Filter from './components/Filter'
import Checkbox from './components/Checkbox'
import Input from './components/Input'
import CurrentWeather from './components/CurrentWeather'
import Forecast from './components/Forecast'
import { useForecast } from './hooks/useForecast';

const Filters = [
    {
        id: 1,
        value: 'Облачно',
    },
    {
        id: 2,
        value: 'Солнечно',
    },
]


export const App = () => {
    const { forecast, selectedForecast, setSelectedForecast } = useForecast();
    const [selectedFilter, setSelectedFilter] = useState<number>();
    const handleClick = (value: number) => {
        setSelectedFilter(value);
    };

    return (
        <main>
            <Filter>
                {Filters.map(filter => (
                    <Checkbox key={filter.id} isChecked={filter.id === selectedFilter} onClick={() => handleClick(filter.id)}>{filter.value}</Checkbox>
                ))}
                <Input name="min-temperature">Минимальная температура</Input>
                <Input name="max-temperature">Максимальная температура</Input>
                <button>Отфильтровать</button>
            </Filter>

            {selectedForecast && <CurrentWeather currentWeather={selectedForecast} /> }
            { forecast && selectedForecast && <Forecast forecast={forecast} selected={selectedForecast} onSelect={setSelectedForecast} /> }
        </main>
    );
};
