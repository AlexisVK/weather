import { format } from "date-fns";
import { ru } from "date-fns/locale";

const FORMAT_OPTIONS = {
    locale: ru
}

export const getWeekDay = (day: number) => {
    return format(day, 'eeee', FORMAT_OPTIONS);
}

export const getDayMonth = (day: number) => {
    return format(day, 'd MMMM', FORMAT_OPTIONS);
}
