export type ForecastType = {
    id: string;
    rain_probability: number;
    temperature: number;
    humidity: number;
    type: 'rainy' | 'sunny' | 'cloudy';
    day: number;
}
