import { useEffect, useState } from 'react';
import { ForecastType } from '../types/index';
import { api } from '../api'

export const useForecast = () => {
    const [forecast, setForecast] = useState<ForecastType[] | null>(null);
    const [selectedForecast, setSelectedForecast] = useState<ForecastType | null>(null);

    // API call
    useEffect(() => {
        (async () => {
            const newForecast = await (await api.getWeather()).json();
            setForecast(newForecast.data)
        })()
    }, []);

    // Set first day as selected when forecast will be available
    useEffect(() => {
        if (!selectedForecast && forecast) {
            setSelectedForecast(forecast[0])
        }
    }, [forecast])

    return { forecast, selectedForecast, setSelectedForecast };
};
